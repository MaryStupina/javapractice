

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

@WebServlet("/temp")
public class ThreadsAndServletsURLParam extends HttpServlet {

//    int i = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        for(int j = 0; j<1000000; j++){
//            i++;
//        }
//        System.out.println((Thread.currentThread().getName())+ " - " + i);

        String one = req.getParameter("one"); // получить один параметр
        String two = req.getParameter("two");
        String three = req.getParameter("three");


        String[] param = req.getParameterValues("one"); // получить несколько параметров под одним название
        System.out.println(one+" "+two+" "+three);

        Enumeration<String> names = req.getParameterNames(); // получить названия параметров
        while(names.hasMoreElements()){                        // и вывести в цикле
            String elementName = names.nextElement();
            System.out.println(elementName+ " = "+ req.getParameter(elementName));
        }

        Map<String, String[]> parametrMap = req.getParameterMap(); // получить параметры и записать в мап
        Iterator<Map.Entry<String, String[]>> iter = parametrMap.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry<String, String[]> znachenia = iter.next();
            String name1 = znachenia.getKey();
            String[] values = znachenia.getValue();
            if(values.length > 1)
                for (String value : values) {
                    System.out.println(name1 +" = " + value);
            }

        }

        System.out.println(req.getRequestURL());// получить URL адрес сервлета
        System.out.println(req.getServletPath());
        System.out.println(req.getRemoteHost());
        System.out.println(req.getRequestURI());
        System.out.println(req.getLocalPort());
        System.out.println(req.getQueryString()); // получить запрос в исходном виде

    }
}