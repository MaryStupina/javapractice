package JDBCTest;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestJDBC {
    public static void main(String[] args) {
        Connection connection = null;
        //URL к базе состоит из протокола:подпротокола://[хоста]:[порта_СУБД]/[БД] и других_сведений
        String url = "jdbc:mysql://localhost:3306/store" +
                "?autoReconnect=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC";
        //Имя пользователя БД
        String name = "root";
        //Имя пользователя БД
        String password = "";

        try {
            //Загружаем драйвер
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Driver is ready!");
            //Создаём соединение
            connection = DriverManager.getConnection(url, name, password);
            System.out.println("Connection is ready!");
            //Для использования SQL запросов существуют 3 типа объектов:
            //1.Statement: используется для простых случаев без параметров
            Statement statement = null;
            statement = connection.createStatement();
            //Выполним запрос
            ResultSet result1 = statement.executeQuery("SELECT * FROM good WHERE id>5 AND id<10");
            //result это указатель на первую строку с выборки
            //чтобы вывести данные мы будем использовать
            //метод next() , с помощью которого переходим к следующему элементу
            System.out.println("Print statement");
            while(result1.next()){
                System.out.println("Number "+ result1.getRow()+ " number in base " +
                        result1.getInt("id") + " good_name = " +
                                result1.getString("name"));
            }
            // Вставить запись
//            statement.executeUpdate("INSERT INTO good(id,name, price) VALUES (326, 'ghd','789') ");
//            System.out.println("Insert done!");

           //Обновить запись
            statement.executeUpdate("UPDATE good SET name = 'uuuuu' WHERE id = 325");
            System.out.println("Update done!");

            //2.PreparedStatement: предварительно компилирует запросы,
            //которые могут содержать входные параметры

            PreparedStatement preparedStatement = null;
            // ? - место вставки нашего значеня
            preparedStatement = connection.prepareStatement("SELECT * FROM category where id > ? and id < ?");
            //Устанавливаем в нужную позицию значения определённого типа
            preparedStatement.setInt(1, 5);
            preparedStatement.setInt(2, 10);
            //выполняем запрос
            ResultSet result2 = preparedStatement.executeQuery();
            System.out.println("Print PreparedStatement");
            while(result2.next()){
                System.out.println("Number "+ result2.getRow()+ " number in base " +
                        result2.getInt("id") + " good_name = " +
                        result2.getString("name"));
            }

            // Вставить запись
            preparedStatement = connection.prepareStatement("INSERT INTO category(id,name) VALUES (72, ?)");
            preparedStatement.setString(1, "sushka3");
            System.out.println(preparedStatement.execute());
            //метод принимает значение без параметров
            //темже способом можно сделать и UPDATE


            //3.CallableStatement: используется для вызова хранимых функций,
            // которые могут содержать входные и выходные параметры
            CallableStatement callableStatement = null;
            //Вызываем функцию myFunc (хранится в БД)
            callableStatement = connection.prepareCall(" { call myfunc(?,?) } ");
            //Задаём входные параметры
            callableStatement.setString(1, "Dima");
            callableStatement.setString(2, "Olga");
            //Выполняем запрос
            ResultSet result3 = callableStatement.executeQuery();
            //Если CallableStatement возвращает несколько объектов ResultSet,
            //то нужно выводить данные в цикле с помощью метода next
            System.out.println(result3.next());
            //если функция вставляет или обновляет, то используется метод executeUpdate()

        } catch (Exception ex) {
            //выводим наиболее значимые сообщения
            Logger.getLogger(TestJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TestJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
