package GenericsPractice;

public class GenericClass<T1, T2> {
    private final T1 left;
    private final T2 right;

    GenericClass(T1 name, T2 value){
        this.right = value;
        this.left = name;
    }

    public T1 getT1(){
        return left;
    }

    public T2 getT2(){
        return right;
    }

    public static void main(String[] args) {
        GenericClass<Integer, String> g = new GenericClass<>(1, "a"); // варианты использования
        Integer data = g.getT1();  // здесь зачение Object из байт-кода кастится к типу Integer

        GenericClass<String, Integer> s = new GenericClass<>("f", 2);
        String str = s.getT1();

        GenericClassExt<String, Integer, Long> sil = new GenericClassExt<>("a", 1, 2l);


    }
}

class GenericClassExt<T1, T2, T3> extends GenericClass<T1, T2>{ // можно наследоваться от параметризированных классов
    private final T3 middle;

    GenericClassExt(T1 name, T2 value, T3 middle) {
        super(name, value);
        this.middle = middle;
    }


}
