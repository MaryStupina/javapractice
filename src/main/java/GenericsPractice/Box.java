package GenericsPractice;

public class Box<T extends Number> { // Т - placeholder, <T extends Number> используется чтобы ограничить
                                                // уровень дженерализирования. На уровне байт-кода T будет уже не Object,
                                    // а типа Number
    private T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
