package GenericsPractice;

public class GenericPractice {
    public static void main(String[] args) {
        Box<Integer> i = new Box<>();
        //Box<String> s = new Box<>();

        // устанавливаем значения объектам
        i.setItem(5);
        //s.setItem("aaa");
    }

    //Если public class Box<T extends Number> - то можем в Вох записать любые типы, которые входят в Number
    //Box<Integer> b1;
    //Box<Float> b2;
    //Box<Number> b3;

    //Использование дженериков в функциях:
    public Number sum(Box<? extends Number> a1, Box<? extends Number> a2){
        return a1.getItem();
    }

    public <T,K,M> T sum(T item1, K item2, M item3){ // инициализируется в момент вызова метода
        return item1;                                // sum(new Integer(5), new Float(2.0), new Double(3.12)){}
    }

}
