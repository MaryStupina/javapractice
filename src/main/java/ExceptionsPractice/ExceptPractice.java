package ExceptionsPractice;

public class ExceptPractice {
    public static void main(String[] args) {
        try {
            metod();
        } catch (MarinaException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void metod() throws MarinaException {
        try {
            throw new MarinaException("1");

        } catch (MarinaException e) {
            System.out.println("Third Line "+e.getMessage());
            throw new MarinaException("2");
        } finally {
            System.out.println("5 Line");
            System.out.println("6 Line");
            throw new MarinaException("3");
        }
    }
}
