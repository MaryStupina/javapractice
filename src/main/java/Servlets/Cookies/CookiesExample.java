package Servlets.Cookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/cookies")
public class CookiesExample extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // отправка cookie клиенту/ создание
        Cookie myCookie = new Cookie("bookName", "java");

        // установить "жизненный цикл" cookie на 24 часа
        myCookie.setMaxAge(60*60*24);// в сек
        resp.addCookie(myCookie); // респонс отправит куки и браузер сам его сохранит


        // извлечение клиентского cookie из HttpServletRequest

        Cookie[] cookies = req.getCookies(); // cookie может быть больше одного

        for(int i = 0; i<cookies.length; i++){ // пробежаться в цикле и получить key-value cookie

           Cookie currentCookie = cookies[i];
           String name = currentCookie.getName();
           String value = currentCookie.getValue();
        }

    }




}
