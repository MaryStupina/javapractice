package MVCPractice.Calculator;

public class Dispatcher {
    public static void main(String[] args) {

        Model m1 = new Model(50, 8);
        Model m2 = new Model(100, 2);

        Controller controller = new Controller();
        int summa = Controller.add(m1);
        View.displayInt(summa);
        int sub = Controller.substract(m2);
        View.displayInt(sub);
}
}
