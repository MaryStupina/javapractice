package MVCPractice.Calculator;

public class Controller {
    static int add(Model m){
        return m.x + m.y;
    }

    static int substract(Model m){
        return m.x - m.y;
    }

    static int mut(Model m){
        return m.x * m.y;
    }

    static double divide(Model m){
        return (double) m.x / m.y;
    }
}
