package General;

import java.util.ArrayList;
import java.util.List;

public class First {
    static List<String> lis;
    static List<List<String>> lisWithList;

    public static void main(String[] args) {
        lis = getList();
        lisWithList = getListWithList();
        System.out.println(sort(lis, lisWithList));


    }

    private static List<String> sort(List<String> lis, List<List<String>> lisWithList){
        List<String> sorted = new ArrayList<>();

        for(int i = 0; i<lis.size(); i++){
            for (int j = 0; j<lisWithList.size(); j++){
                for(int d = 0; d < lisWithList.get(j).size(); d++) {
                    if (lis.get(i).equals(lisWithList.get(j).get(d))) {
                        sorted.add(lis.get(i));
                    }
                }
            }
        }
        return sorted;
    }

    private static List<String> getList(){
        List<String> newList = new ArrayList<>();
        newList.add("ggg");
        newList.add("ggg2");
        newList.add("fff");
        return newList;
    }

    private static List<List<String>> getListWithList(){
        List<List<String>> listWithList = new ArrayList<>();
        List<String>  simpleList = new ArrayList<>();
        simpleList.add("ggg");
        simpleList.add("vv2");
        simpleList.add("vv3");
        listWithList.add(simpleList);

        List<String>  simpleList2 = new ArrayList<>();
        simpleList2.add("fff");
        simpleList2.add("ggg");
        simpleList2.add("ff3");
        listWithList.add(simpleList2);

        return listWithList;
    }


}
