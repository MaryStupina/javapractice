package AnnotationReflection.Ex1;

import java.lang.reflect.Field;

public class Validator {

    public static void validate(Object o) throws IllegalAccessException {
        Field[] declaredFields = o.getClass().getDeclaredFields();

        for(Field field : declaredFields){
            field.setAccessible(true);

            Longer annotation = field.getDeclaredAnnotation(Longer.class);

            if(annotation != null){
                int maxLenth = annotation.lenth();
                if(((String)field.get(o)).length() > maxLenth){
                    throw new RuntimeException("To high value");
                }
            }
        }
    }
}
