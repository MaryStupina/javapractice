package AnnotationReflection.AnnotationCreationExampl;

@Servise(name = "superLazy")
public class LazyServise {

    @Init
    public void lazyInit() throws Exception{
        System.out.println("lazy service");
    }
}
