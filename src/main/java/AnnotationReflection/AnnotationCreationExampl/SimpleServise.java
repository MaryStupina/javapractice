package AnnotationReflection.AnnotationCreationExampl;

@Servise(name = "superSimple")
public class SimpleServise {

    @Init
    public void initService(){
        System.out.println("init service");
    }

    public void notInit(){
        System.out.println("without init annotation");
    }
}
