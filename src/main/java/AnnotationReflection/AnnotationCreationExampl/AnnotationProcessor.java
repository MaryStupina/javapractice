package AnnotationReflection.AnnotationCreationExampl;

public class AnnotationProcessor {
    public static void main(String[] args) {

        inspectService(SimpleServise.class);
        inspectService(LazyServise.class);
        inspectService(String.class);
    }

     static void inspectService(Class<?> service){
        if(service.isAnnotationPresent(Servise.class)){
            Servise ann = service.getAnnotation(Servise.class);
            System.out.println(ann.name());
            System.out.println(ann.lazyLoad());

        }else{
            System.out.println("sorry, annotations are not found");
        }
    }
}