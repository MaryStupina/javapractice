package AnnotationReflection.AnnotationCreationExampl;

import java.lang.annotation.*;


    //эти аннотации можно указать только над другими аннотациями

    @Retention(RetentionPolicy.RUNTIME)// время жизни - будет существовать также и в рантайме
    @Target(ElementType.TYPE)// область применения - над всеми классами и интерфейсами
    @Inherited// наследуется потомками класса
    @Documented //документ/класс/аннотация если помечена - попадает в JavaDock
    public @interface Servise{
        String name(); // свойство аннотации - обязательно указывать имя при создании

        boolean lazyLoad() default false;// значение по умолчанию false
    }

