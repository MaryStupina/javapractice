package AnnotationReflection.Reflaction;

public class Employee extends Person {
    @Override
    public void raiseSalary() {
        System.out.println("Raising salary for employee " + this);
    }
}
