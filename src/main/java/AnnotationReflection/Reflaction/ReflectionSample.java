package AnnotationReflection.Reflaction;

import java.lang.reflect.Method;

public class ReflectionSample {

    public static void main(String[] args) {
        Class clazz = Employee.class ;// загружаем класс, о котором нужно получить данные
        System.out.println(clazz);

        Method method [] = clazz.getDeclaredMethods(); // получить инфо о методах класса
        for (int i = 0; i<method.length; i++) {
            System.out.println("the Employee method signature is: "+ method[i].toString());
        }

        Class superClass = clazz.getSuperclass(); // узнаем суперкласс
        System.out.println("The name of superClass is " + superClass.getName());

        Method superMethods[] = superClass.getMethods(); //получить инфо о методах супер класса
        for (int i = 0; i<superMethods.length; i++) {
            System.out.println("the superClass method signature is: "+ superMethods[i].toString());
            System.out.println("Return type is "+ superMethods[i].getReturnType().getName());
            System.out.println("");
        }
    }
}
