package AnnotationReflection;

public @interface AnnExample {
    String name();
    String type() default "string";
}
