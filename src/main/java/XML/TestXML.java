package XML;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class TestXML {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {

        saxParse();

    }

    public static void saxParse() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory f = SAXParserFactory.newInstance();
        SAXParser p = f.newSAXParser();


        DefaultHandler h = new DefaultHandler() {

            public void startElement(String uri, String name, String qName, Attributes atr) {
            }

            void endElement() {
            }

            public void characters(char[] ch, int start, int length) {
            }
        };
        p.parse("/Users/macbookpro/IdeaProjects/testProject2/src/main/java/XML/test.xml", h);
        System.out.println("ok");
    }
}
