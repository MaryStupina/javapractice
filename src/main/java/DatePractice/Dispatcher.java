package DatePractice;
import java.util.*;

public class Dispatcher {
    public static void main(String[] args) {

        GregorianCalendar gc = new GregorianCalendar();
//        System.out.println(gc.get(Calendar.DAY_OF_MONTH));
//        System.out.println(gc.get(Calendar.MONTH));
//        System.out.println(gc.get(Calendar.DAY_OF_YEAR));
//        System.out.println(gc.get(Calendar.DAY_OF_WEEK));

        gc.set(Calendar.MONTH, Calendar.SEPTEMBER);
        gc.set(Calendar.DAY_OF_MONTH, 24);
//        System.out.println(gc.get(Calendar.DAY_OF_WEEK));

        GregorianCalendar gc1 = new GregorianCalendar(2019, Calendar.JANUARY, 1);
//        System.out.println(gc1.get(Calendar.DAY_OF_WEEK));

        Date d = gc1.getTime(); // convert to Date
        gc.setTime(d);
    }
}
