package DatePractice;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarTest {
    public static void main(String[] args) {
        // создание объекта, представляющего текущую дату
        GregorianCalendar gc = new GregorianCalendar();
        int today = gc.get(Calendar.DAY_OF_MONTH);
        int month = gc.get(Calendar.MONTH);

        // установка объекта на первое число месяца
        gc.set(Calendar.DAY_OF_MONTH, 1);
        int weekday = gc.get(Calendar.DAY_OF_WEEK);

        // вывод заголовка таблицы
        System.out.println("Mon Tue Wed Thu Fri Sat Sun");
        // при отображении строки календаря возможен сдвиг
        for (int i=Calendar.MONDAY; i<weekday; i++){
            System.out.print("    ");
        }

        do
            {
               // вывод дня месяца
               int day = gc.get(Calendar.DAY_OF_MONTH);
                System.out.printf("%3d", day);
                // символом * помечаем текущий день
                if(day == today){
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                }

                // после каждого воскресенья начинается новая строка
                if(weekday == Calendar.SUNDAY){
                    System.out.println();
                }
                // перевод gc на следующий день
                gc.add(Calendar.DAY_OF_MONTH, 1);
                weekday = gc.get(Calendar.DAY_OF_WEEK);
            }

        while (gc.get(Calendar.MONTH) == month);
        // цикл завершается, когда gc установлен на первый день следующего месяца
        // при необходимости переводится строка

        if(weekday != Calendar.MONDAY){
            System.out.println();
        }
    }
}
