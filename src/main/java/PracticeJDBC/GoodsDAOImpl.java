package PracticeJDBC;

import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GoodsDAOImpl implements GoodDAO{
    static
    {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    Connection connection = null;
    String url = "jdbc:mysql://localhost:3306/store" +
            "?autoReconnect=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC";
    String login = "root";
    String password = "";

    public Goods findById(int idd) {

        PreparedStatement ps = null;
        int idResultFind = 0;
        int countResultFind = 0;
        String nameResultFind = null;

        try {
            connection = DriverManager.getConnection(url, login, password);
            ps = connection.prepareStatement("SELECT id, name, count FROM goods WHERE id = ?");
            ps.setInt(1, idd);
            ResultSet resultFind = ps.executeQuery();

            while(resultFind.next()) {
                idResultFind = resultFind.getInt("id");
                countResultFind = resultFind.getInt("count");
                nameResultFind = resultFind.getString("name");
            }
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closing(ps);
        }
        Goods goodFind = new Goods(idResultFind, countResultFind, nameResultFind);
        return goodFind;
    }


    public void deleteById(int idd){
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(url, login, password);
            ps = connection.prepareStatement("DELETE FROM goods WHERE id = ?");
            ps.setInt(1, idd);
            ps.executeUpdate();
            if(ps.executeUpdate() == 0) {
                System.out.println("Товар успешно удален");
                ps.close();
            }else{
                System.out.println("Произошла ошибка при удалении товара");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closing(ps);
        }
    }

    public void addGoods(int ccount, String name){
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(url, login, password);
            ps = connection.prepareStatement("INSERT INTO goods(name,count) VALUES (?,?)");
            ps.setInt(2, ccount);
            ps.setString(1, name);
            if(ps.executeUpdate() == 1) {
                System.out.println("Товар успешно добавлен");
            }else{
                System.out.println("Произошла ошибка при добавлении товара");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closing(ps);
        }
    }

    public void addGoods2(List <Goods> list){
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(url, login, password);
            ps = connection.prepareStatement("INSERT INTO goods(name, count) VALUES (?,?)");
            for(int i =0; i<list.size(); i++) {
                ps.setString(1, list.get(i).getName());
                ps.setInt(2, list.get(i).getCount());
                ps.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closing(ps);
        }

    }

    public List<Goods> findAll(){
        PreparedStatement ps = null;
        List<Goods> findAllGoods = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(url, login, password);
            ps = connection.prepareStatement("SELECT * FROM goods");
            ResultSet resultAllFind = ps.executeQuery();

            while(resultAllFind.next()) {
                findAllGoods.add(new Goods(resultAllFind.getInt("id"),
                        resultAllFind.getInt("count"),
                        resultAllFind.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closing(ps);
        }
        return findAllGoods;
    }

    public void closing(PreparedStatement ps){
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        if(ps != null){
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

