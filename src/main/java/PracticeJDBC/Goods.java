package PracticeJDBC;

import java.io.Serializable;

public class Goods implements Serializable{
    private int id;
    private int count;
    private String name;


    public Goods(int idResultFind, int countResultFind, String nameResultFind) {
        this.id = idResultFind;
        this.count = countResultFind;
        this.name = nameResultFind;
    }

    public Goods(){

    }

    public void setId(int id){
        this.id = id;
    }
    public void setCount(int count){
        this.count = count;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getId(){
        return id;
    }
    public int getCount(){
        return count;
    }
    public String getName(){
        return name;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", count=" + count +
                ", name='" + name + '\'' +
                '}';
    }
}
