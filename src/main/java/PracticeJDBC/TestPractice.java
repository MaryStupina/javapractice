package PracticeJDBC;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TestPractice {
    static {
        System.out.println("Введите значение: 1 - добавить товар, " +
                "2 - найти товар по №, 3 - удалить товар по №, 4 - найти все товары, " +
                "5 - экспорт базы товаров, 6 - импорт базы товаров");
    }
    private static GoodDAO g = new GoodsDAOImpl();

    public static void main(String[] args) {
        try {
            try (InputStreamReader in = new InputStreamReader(System.in);
                 BufferedReader b = new BufferedReader(in);) {

                int s = 0;
                try {
                    s = Integer.parseInt(b.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                switch (s) {
                    case 1:
                        addGood();
                        break;
                    case 2:
                        findGood();
                        break;
                    case 3:
                        deleteGood();
                        break;
                    case 4:
                        findAllGoods();
                        break;
                    case 5:
                        exportBase();
                        break;
                    case 6:
                        importBase();
                        break;
                    default:
                        System.out.println("Вы ввели неверное число");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addGood(){
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader b = new BufferedReader(in);
        int count = 0;
        String name = null;
        System.out.println("Введите количество");
            try {
                count = Integer.parseInt(b.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        System.out.println("Введите название товара");
            try {
                name = b.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        g.addGoods(count, name);
        try {
            b.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void findGood(){
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader b = new BufferedReader(in);
        int id = 0;
        System.out.println("Введите id");
            try {
                id = Integer.parseInt(b.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        System.out.println(g.findById(id));
            try {
                b.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void deleteGood(){
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader b = new BufferedReader(in);
        int id = 0;
        System.out.println("Введите id");
            try {
                id = Integer.parseInt(b.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        g.deleteById(id);
            try {
                b.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void findAllGoods(){
        List<Goods> f = g.findAll();
        for(int i = 0; i<f.size(); i++){
            System.out.println(f.get(i));
        }
    }

    public static void exportBase(){
        try(FileOutputStream fos = new FileOutputStream("/Users/macbookpro/Desktop/JAVA/Test/DBGoods.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            GoodDAO g = new GoodsDAOImpl();
            List<Goods> f = g.findAll();
            oos.writeObject(f);
            oos.flush();
            System.out.println("База успешно экспортирована");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void importBase(){
        try(FileInputStream fis = new FileInputStream("/Users/macbookpro/Desktop/JAVA/Test/DBGoods.txt");
            ObjectInputStream ois = new ObjectInputStream(fis)) {

            List<Goods>  gg = (ArrayList) ois.readObject();
            g.addGoods2(gg);

            System.out.println("База товаров успешно импортирована");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}