package PracticeJDBC;

import java.util.List;

public interface GoodDAO {
    public Goods findById(int idd);

    public void deleteById(int idd);

    public void addGoods(int ccount, String name);

    public void addGoods2(List <Goods> list);

    public List<Goods> findAll();
}
