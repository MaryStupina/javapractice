package MultithreadingPractice;

import java.util.concurrent.Exchanger;

public class ExchangerExample {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<>();

        new Mike(exchanger).start();
        new Anketa(exchanger).start();

    }

    static class Mike extends Thread{
        Exchanger<String> exchanger;

        public Mike(Exchanger<String> exchanger){
            this.exchanger = exchanger;
        }

        @Override
        public void run(){
            try {
                exchanger.exchange("Hi, my name is Mike");
                sleep(1000);
                exchanger.exchange("I'm 20 years old");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    static class Anketa extends Thread{
        Exchanger<String> exchanger;

        public Anketa(Exchanger<String> exchanger){
            this.exchanger = exchanger;
        }

        @Override
        public void run(){
            try {
                System.out.println(exchanger.exchange(null));
                System.out.println(exchanger.exchange(null));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



}
