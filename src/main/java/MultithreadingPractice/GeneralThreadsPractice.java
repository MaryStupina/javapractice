package MultithreadingPractice;

public class GeneralThreadsPractice {

    public static class MyThread implements Runnable {

        @Override
        public void run() {
            System.out.println("Thread created from implements Runnable");
        }
    }

    public static class MyThread2 extends Thread{

        @Override
        public void run() {
            System.out.println("Thread created from extends Thread");
        }
    }

    public static void main(String[] args) {

        Thread t = new Thread(new MyThread());// передаем в поток наш класс
        t.start(); // вызываем только через старт!
    }



}
