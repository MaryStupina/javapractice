package MultithreadingPractice;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(3);

        new Work(countDownLatch).start();
        new Work(countDownLatch).start();
        new Work(countDownLatch).start();

        countDownLatch.await();
        System.out.println("All work done!");
    }
}

class Work extends Thread{
    CountDownLatch countDownLatch;

    public Work(CountDownLatch countDownLatch){
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run(){
        try {
            sleep(3000);
            System.out.println("work done!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        countDownLatch.countDown();
    }


}
