package MultithreadingPractice;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorService {
    public static void main(String[] args) {
        java.util.concurrent.ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.schedule(new MyThread(), 3, TimeUnit.SECONDS);
        scheduledExecutorService.shutdown();


    }

    static class MyThread extends Thread{
        @Override
        public void run(){
            System.out.println(1);
        }
    }
}
