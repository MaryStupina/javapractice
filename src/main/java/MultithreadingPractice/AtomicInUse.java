package MultithreadingPractice;

import java.util.HashMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicInUse {

    static AtomicInteger count = new AtomicInteger(0);
//    static int count = 0;


    public static void main(String[] args) {
        for (int i = 0; i<100000; i++){
            Thread first = new MyAtomicThread();
//            Thread first = new SimplInt();
            first.start();
        }
    }

    static class MyAtomicThread extends Thread {
        @Override
        public void run(){
            int u = count.incrementAndGet();
            System.out.println(u);
        }
    }

//    static class SimplInt extends Thread {
//        @Override
//        public void run(){
//            count++;
//            System.out.println(count);
//        }
//    }
}



