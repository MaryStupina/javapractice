package MultithreadingPractice;

import sun.jvm.hotspot.utilities.soql.Callable;

import javax.script.ScriptException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CallableFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        java.util.concurrent.Callable<Integer> callable = new MyCallable();
        FutureTask futureTask = new FutureTask(callable);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
    }

    static class MyCallable implements java.util.concurrent.Callable<Integer>{

        @Override
        public Integer call() throws ScriptException {
            int j = 0;
            for(int i = 0; i<10; i++, j++){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return j;
        }

    }
}
