package MultithreadingPractice;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrierbarrier = new CyclicBarrier(3, new Run());

        new Spotsman(cyclicBarrierbarrier).start();
        new Spotsman(cyclicBarrierbarrier).start();
        new Spotsman(cyclicBarrierbarrier).start();
    }

    static class Run extends Thread{
        @Override
        public void run(){
            System.out.println("All ready. Run has begun!");
        }
    }

    static class Spotsman extends Thread{
        CyclicBarrier barrier;

        public Spotsman(CyclicBarrier barrier){
            this.barrier = barrier;
        }

        @Override
        public void run(){
            try {
                barrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}
