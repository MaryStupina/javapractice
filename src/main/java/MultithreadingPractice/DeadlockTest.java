package MultithreadingPractice;

public class DeadlockTest {
    static final Object lock1 = new Object();
    static final Object lock2 = new Object();

    public static void main(String[] args) {
        DeadThread1 dt1 = new DeadThread1();
        DeadThread2 dt2 = new DeadThread2();

        dt1.start();
        dt2.start();


    }
public static class DeadThread1 extends Thread{
        public void run(){
            synchronized (lock1){
                System.out.println("DeadThread1 is holding lock1...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("DeadThread1 is waiting for lock2...");
                synchronized (lock2){
                    System.out.println("DeadThread1  is holding lock1 and lock2...");
                }
            }
        }
}

private static class DeadThread2 extends Thread {
        public void run() {
            synchronized (lock2) {
                System.out.println("DeadThread2 is holding lock2...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("DeadThread2 is waiting for lock1...");
                synchronized (lock1) {
                    System.out.println("DeadThread2  is holding lock1 and lock2...");
                }
            }
        }
    }
}

