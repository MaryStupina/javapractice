package PracticeSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {

    // случайный порт (может быть любое число от 1025 до 65535)
    int port = 6666;
    // создаем сокет сервера и привязываем его к вышеуказанному порту
        try {
            ServerSocket ss = new ServerSocket(port);
            System.out.println("Waiting for a client...");
            // заставляем сервер ждать подключений и выводим сообщение когда кто-то связался с сервером
            Socket socket = ss.accept();
            System.out.println("Got a client :) ... Finally, someone saw me through all the cover!");
            System.out.println();

            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиенту.
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();

            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
            DataInputStream din = new DataInputStream(is);
            DataOutputStream dos = new DataOutputStream(os);

            String line = null;
            while(true){
                line = din.readUTF();// ожидаем пока клиент пришлет строку текста.
                System.out.println("The client just sent me a line");
                System.out.println("I'm sending it back...");
                dos.writeUTF(line.toUpperCase()+" 101"); // отсылаем клиенту обратно ту самую строку текста

                dos.flush(); // заставляем поток закончить передачу данных.
                System.out.println("Waiting for the next line...");
                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
