package PracticeSocket;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) {

        int serverPort = 6666; // здесь обязательно нужно указать порт к которому привязывается сервер.
        String address = "localhost"; // это IP-адрес компьютера, где исполняется наша серверная программа.

        try {
            // создаем объект который отображает вышеописанный IP-адрес
            InetAddress ipAddress = InetAddress.getByName(address);
            System.out.println("Done ipAddress");
            Socket socket = new Socket(ipAddress, serverPort);// создаем сокет используя IP-адрес и порт сервера.

            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом.
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения
            DataInputStream dis = new DataInputStream(is);
            DataOutputStream dos = new DataOutputStream(os);

            // Создаем поток для чтения с клавиатуры
            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            String line = null;
            System.out.println("Type smth and press enter");

            while(true){
                line = keyboard.readLine(); // ждем пока пользователь введет что-то и нажмет кнопку Enter.
                if("break".equals(line)){
                    break;
                }

                System.out.println("Sending this line to the server...");
                dos.writeUTF(line); // отсылаем введенную строку текста серверу.
                dos.flush(); // заставляем поток закончить передачу данных.
                line = dis.readUTF(); // ждем пока сервер отошлет строку текста.
                System.out.println("The server says: " +line);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
