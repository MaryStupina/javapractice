package PracticeJNDI;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class GettingInitialContext {

    public static void main(String[] args) throws NamingException {

        // java program inside the app server
        Context namingContext = new InitialContext();

        // java program outside of the app server (a Glassfish-speciffic example)
        final Properties props = new Properties();
        // jndi properties are not the same in every java EE server

        props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
        props.setProperty("java.naming.factory.url.pkgs","com.sun.enterprise.naming");
        props.setProperty("java.naming.factory.state",
                "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
        props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
        props.setProperty("org.omg.CORBA.ORBInitialPort", "8080");
        InitialContext ic = new InitialContext(props);

        //связать имя с каким-то файлом
        String name = "/home/java/data.txt";
        Properties prop = new Properties();
        prop.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        prop.put(Context.PROVIDER_URL, "file:///");

        Context initialCont = new InitialContext(prop);
        
    }
}
