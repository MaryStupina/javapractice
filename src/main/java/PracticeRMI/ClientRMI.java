package PracticeRMI;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientRMI {
    public static void main(String[] args) throws RemoteException, NotBoundException {
        // проверяем контроль доступа к локальным ресурсам
        if(System.getSecurityManager() == null){
            System.setSecurityManager(new SecurityManager());
        }

            // создаем ссылку на удаленный реестр, который находится на нашем сервере
            Registry registry = LocateRegistry.getRegistry("127.0.0.1");

            // получаем ссылку на удаленный объект из реестра
            MyRemoteInterface stub = (MyRemoteInterface) registry.lookup("MyRemoteObject");

        System.out.println("Increased number is " + stub.getIncreaseNumber(1));
    }
}
