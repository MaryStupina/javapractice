package PracticeRMI;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerRMI {

    public static void main(String[] args) {
        // определяем SecurityManager - отвечает за контроль доступа к локальным ресурсам со стороны загружаемого кода
        // .java.policy - определяет политики доступа, необходимо передавать в качестве параметра JVM сервера и
        // клиента
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        // создаем объект
        MyRemoteInterface myRemoreObject = new MyRemoteObject();

        try {
            // определяем заглушку, с которой будет взаимодействовать определнный клиент
            // для получения заглушки передается только что созданный объект и значение порта. 0 означает, что
            // среда сама выберет порт, по которому объект будет принимать вызовы
            MyRemoteInterface stub = (MyRemoteInterface) UnicastRemoteObject.exportObject(myRemoreObject, 0);
            //определяем реестр именований удаленных объектов
            // 1099 - порт по умолчанию
            Registry registry = LocateRegistry.createRegistry(1099);

            // регистрируем объект/заглушку в реестре
            registry.bind("MyRemoteObject", stub);
            System.out.println("Bound MyRemoteObject");
        } catch (Throwable e) {
            System.out.println("Couldn't bound MyRemoteObject cause "+ e.getMessage());
        }
    }
}

interface MyRemoteInterface extends Remote{
        Integer getIncreaseNumber(Integer number) throws RemoteException; // обязательно должен выбрасывать исключение
    }

class MyRemoteObject implements MyRemoteInterface{

    @Override
    public Integer getIncreaseNumber(Integer number) throws RemoteException {
        return number+1;
    }
}


