package AnnotationReflection.Ex1;

public class TestEntity{

    @Longer(lenth = 1)
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public TestEntity(String name) {
        this.name = name;
    }
}
