package AnnotationReflection.Ex1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ValidatorTest {

    TestEntity testEntity = new TestEntity("t");

    @Test
    public void validate() throws Exception {
        testEntity.setName("y");
        Validator.validate(testEntity);
        }

    @Test(expected = RuntimeException.class)
    public void validateError() throws Exception {
        testEntity.setName("hhhgghhhhghh");
        Validator.validate(testEntity);
    }

}