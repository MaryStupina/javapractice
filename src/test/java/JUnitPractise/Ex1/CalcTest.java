package JUnitPractise.Ex1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CalcTest {

    private Calc calc;

    @Before
    public void init(){
        calc = new Calc();

        SomeClass sc = mock(SomeClass.class);
        when(sc.result(2)).thenReturn(15);
//        when(sc.result(anyInt())).thenReturn(13);

        calc.someClass = sc;
    }

    @Test
    public void add() throws Exception {
        int res = calc.add(2,3);
//        Assert.assertTrue(res == 5);
        Assert.assertEquals(18, res);
    }

    @Test
    public void div() throws Exception {
        double res = calc.div(6,3);
        Assert.assertTrue(res == 2);
    }

}